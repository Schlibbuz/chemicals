/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.chemicals;

import java.util.Objects;

/**
 *
 * @author stefan
 */
public class Temperature {
    private String type;
    private Double value;

    public Temperature() {

    }


    public Temperature(String type) {
        this.type = type;
        value = null;
    }


    public Temperature(Double value) {
        type = "normal";
        this.value = value;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public Double value() {
        return value;
    }


    public void value(Double value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "Temperature{" + "type=" + type + ", value=" + value + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.type);
        hash = 11 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Temperature other = (Temperature) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return Objects.equals(this.value, other.value);
    }

}
