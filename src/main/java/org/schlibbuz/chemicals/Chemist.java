/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.chemicals;

import java.io.IOException;
import java.util.Random;

/**
 *
 * @author stefan
 */
public class Chemist {
    /**
     * 1=errors, 2=warnings, 3=verbose
     */
    private final int loglevel;
    
    private static Chemist chemist;
    
    private final PeriodicSystem periodicSystem;

    
    public Chemist() {
        loglevel = 0;
        periodicSystem = PeriodicSystem.getInstance();
    }


    public Chemist(int loglevel) {
        this.loglevel = loglevel;
        periodicSystem = PeriodicSystem.getInstance();
    }


    /**
     * @param args
     * @throws java.io.IOException
     */
    public static void main(String args[]) throws IOException {
        chemist = new Chemist(3);

        for (int i = 0; i < 2000; i += 1) {
            chemist.work();
        }
    }


    private void work() {
        Element element = periodicSystem.getRandomElement();
        Double randomTemp = chemist.getRandomTempCelsius();
        if (loglevel > 2) {
            System.out.println("testing element @ " + randomTemp + " Celsius");
            System.out.println(element);
        }
        element.setTempCelsius(randomTemp);
        chemist.analyzeElement(element);
    }


    /**
     * Returns a random temperature between -273 and 6000
     * @return the {@code Double} with the temperature
     */
    private Double getRandomTempCelsius() {
        return (double)new Random().nextInt(6274)-273;
    }


    /**
     * Prints the physical state of a chemical element at a given temperature
     * @param element
     *      the {@code Element} containing the representation
     */
    protected void analyzeElement(final Element element) {
        System.out.println("The physical state of " + element.getName() + " at " + element.getTempCelsius() + " Celsius is " + element.getPhysicalState());
    }


    /**
     * Prints the physical state of a chemical element at a given temperature
     * @param element
     *      the {@code Element} containing the representation
     * @param temperature
     *      the temperature in celsius
     */
    protected void analyzeElement(final Element element, final Double temperature) {
        element.setTempCelsius(temperature);
        analyzeElement(element);
    }

}
