/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.chemicals;

import java.util.Objects;

/**
 *
 * @author stefan
 */
public class Element {
    //atomic number
    private final int an;
    private final String shortName = null;
    private final String name;
    // melting point
    private final Temperature mp;
    //boiling point
    private final Temperature bp;
    // current temperature in celsius
    private final Temperature tempCelsius;

    public Element(int an, String name, Temperature mp, Temperature bp) {
        this.an = an;
        this.name = name;
        this.mp = mp;
        this.bp = bp;
        tempCelsius = new Temperature(20.0d);
    }
    
    public String getPhysicalState() {
        
        //arsen has no liqiud form
        if (bp.getType().equals("sublimiert")) {
            if (tempCelsius.value() < mp.value()) {
                return "solid";
            } else {
                return "gaseous";
            }
        }

        if (mp.getType().equals("n.a.") || tempCelsius.value() < mp.value()) {
            return "solid";
        }
        

        if (bp.getType().equals("n.a.") || tempCelsius.value() < bp.value()) {
            return "fluid";
        }
        
        return "gaseous";
    }
    
    

    public int getAn() {
        return an;
    }

    public String getShortName() {
        return shortName;
    }

    public String getName() {
        return name;
    }

    public Temperature getMp() {
        return mp;
    }

    public Temperature getBp() {
        return bp;
    }

    public Double getTempCelsius() {
        return tempCelsius.value();
    }

    public void setTempCelsius(Double tempCelsius) {
        this.tempCelsius.value(tempCelsius);
    }

    @Override
    public String toString() {
        return "Element{" + "an=" + an + ", shortName=" + shortName + ", name=" + name + ", mp=" + mp + ", bp=" + bp + ", tempCelsius=" + tempCelsius + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.an;
        hash = 29 * hash + Objects.hashCode(this.shortName);
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.mp);
        hash = 29 * hash + Objects.hashCode(this.bp);
        hash = 29 * hash + Objects.hashCode(this.tempCelsius);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Element other = (Element) obj;
        if (this.an != other.an) {
            return false;
        }
        if (!Objects.equals(this.shortName, other.shortName)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.mp, other.mp)) {
            return false;
        }
        if (!Objects.equals(this.bp, other.bp)) {
            return false;
        }
        return Objects.equals(this.tempCelsius, other.tempCelsius);
    }

}
