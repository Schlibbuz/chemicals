/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.chemicals;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author stefan
 */
public final class PeriodicSystem {
    private static final int LOG_LEVEL = 3;

    private static PeriodicSystem periodicSystem;

    private final Map<Integer, Element> elements;

    private PeriodicSystem() {
        elements = new HashMap<>();
        initialize();
    }

    public static synchronized PeriodicSystem getInstance() {
        if (periodicSystem == null) {
            periodicSystem = new PeriodicSystem();
        }
        return periodicSystem;
    }

    private void initialize() {
        try {
            List<String> lines = FileUtils.readLines(new File("data"), "utf-8");

            lines.stream().map((line) -> line.split("\t")).map((lineParts) ->
                    createElementFromStringArray(lineParts))
                    .forEachOrdered((Element element) -> {
                        elements.put(element.getAn(), element);
            });

            if (LOG_LEVEL > 2) {
                elements.entrySet().forEach((entry) -> {
                    System.out.println(entry.getKey() + "/" + entry.getValue());
                });
            }
        } catch(IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Helper function for {@code createElementsMap()}
     * @param lineParts
     *      the {@String[]} containing the element representation
     * @return the {@code Element} represented by the string[] argument
     */
    private Element createElementFromStringArray(final String lineParts[]) {
        int an = Integer.parseInt(lineParts[0].trim());
        String name = lineParts[1].trim();
        
        Temperature mp = createTemperatureFromString(lineParts[2].trim());
        Temperature bp = createTemperatureFromString(lineParts[3].trim());
        
        return new Element(an, name, mp, bp);

    }

    /**
     * Parses a temperature from a string representation
     * @param temperature
     *      the {@code String} containing the temperature representation to be parsed
     * @return the {@code Temperature} represented by the string argument
     */
    private Temperature createTemperatureFromString(final String temperature) {
        String type = "normal";
        
        if (temperature.equals("k.A.")) {
            return new Temperature("n.a.");
        }
        
        try {
            return new Temperature(Double.parseDouble(temperature));
        } catch(NumberFormatException e) {
            return new Temperature(temperature);
        }
    }

    /**
     * Returns the chemical element for a given atomic number
     * @param an
     *      the {@code int} with the atomic number
     * @return the {@code Element}
     */
    protected Element getElement(final int an) {
        return elements.get(an);
    }

    /**
     * Returns a random element from the periodic system
     * @return the {@code Element}
     */
    protected Element getRandomElement() {
        return getElement(new Random().nextInt(elements.size()) + 1);
    }
    
}
