/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.chemicals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author stefan
 */
public class ChemistTest {
    
    public ChemistTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Chemist.
     * @throws java.lang.Exception
     */
    @Test
    public void testMain() throws Exception {
        System.out.println("main");
        String[] args = null;
        Chemist.main(args);
    }

    /**
     * Test of analyzeElement method, of class Chemist.
     */
    @Test
    public void testAnalyzeElement() {
        Chemist instance = new Chemist();
        Element element = new Element(
                33,
                "Arsen",
                new Temperature(613d),
                new Temperature("sublimiert")
        );
        instance.analyzeElement(element, 50d);
    }
    
}
