/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.chemicals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stefan
 */
public class ElementTest {
    
    public ElementTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPhysicalState method, of class Element.
     */
    @Test
    public void testGetPhysicalState() {
        System.out.println("getPhysicalState");
        Element instance = new Element(
                49,
                "Indium",
                new Temperature(156.2d),
                new Temperature(2080d)
        );
        String expResult = "solid";
        String result = instance.getPhysicalState();
        assertEquals(expResult, result);
        
        instance.setTempCelsius(200d);
        expResult = "fluid";
        result = instance.getPhysicalState();
        assertEquals(expResult, result);

        instance.setTempCelsius(3000d);
        expResult = "gaseous";
        result = instance.getPhysicalState();
        assertEquals(expResult, result);
        
        instance = new Element(
                33,
                "Arsen",
                new Temperature(613d),
                new Temperature("sublimiert")
        );
        expResult = "solid";
        result = instance.getPhysicalState();
        assertEquals(expResult, result);

        instance.setTempCelsius(700d);
        expResult = "gaseous";
        result = instance.getPhysicalState();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAn method, of class Element.
     */
    @Test
    public void testGetAn() {
        System.out.println("getAn");
        Element instance = new Element(
                49,
                "Indium",
                new Temperature(156.2d),
                new Temperature(2080d)
        );
        int expResult = 49;
        int result = instance.getAn();
        assertEquals(expResult, result);
    }

    /**
     * Test of getShortName method, of class Element.
     */
    @Test
    public void testGetShortName() {
        System.out.println("getShortName");
        Element instance = new Element(
                49,
                "Indium",
                new Temperature(156.2d),
                new Temperature(2080d)
        );
        String expResult = null;
        String result = instance.getShortName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class Element.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Element instance = new Element(
                49,
                "Indium",
                new Temperature(156.2d),
                new Temperature(2080d)
        );
        String expResult = "Indium";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMp method, of class Element.
     */
    @Test
    public void testGetMp() {
        System.out.println("getMp");
        Element instance = new Element(
                49,
                "Indium",
                new Temperature(156.2d),
                new Temperature(2080d)
        );
        Temperature expResult = new Temperature(156.2d);
        Temperature result = instance.getMp();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBp method, of class Element.
     */
    @Test
    public void testGetBp() {
        System.out.println("getBp");
        Element instance = new Element(
                49,
                "Indium",
                new Temperature(156.2d),
                new Temperature(2080d)
        );
        Temperature expResult = new Temperature(2080d);
        Temperature result = instance.getBp();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTempCelsius method, of class Element.
     */
    @Test
    public void testGetTempCelsius() {
        System.out.println("getTempCelsius");
        Element instance = new Element(
                49,
                "Indium",
                new Temperature(156.2d),
                new Temperature(2080d)
        );
        Double expResult = 20d;
        Double result = instance.getTempCelsius();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTempCelsius method, of class Element.
     */
    @Test
    public void testSetTempCelsius() {
        System.out.println("setTempCelsius");
        Double tempCelsius = 50d;
        Element instance = new Element(
                49,
                "Indium",
                new Temperature(156.2d),
                new Temperature(2080d)
        );
        instance.setTempCelsius(tempCelsius);
    }

    /**
     * Test of toString method, of class Element.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Element instance = new Element(
                34,
                "Selen",
                new Temperature(217d),
                new Temperature(685d)
        );
        String expResult = "Element{an=34, shortName=null, name=Selen, mp=Temperature{type=normal, value=217.0}, bp=Temperature{type=normal, value=685.0}, tempCelsius=Temperature{type=normal, value=20.0}}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
