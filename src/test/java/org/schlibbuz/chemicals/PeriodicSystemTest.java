/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.chemicals;

import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stefan
 */
public class PeriodicSystemTest {
    
    public PeriodicSystemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class PeriodicSystem.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        PeriodicSystem expResult = null;
        PeriodicSystem result = PeriodicSystem.getInstance();
        assertTrue(result instanceof PeriodicSystem);
    }

    /**
     * Test of getElement method, of class PeriodicSystem.
     */
    @Test
    public void testGetElement() {
        System.out.println("getElement");
        int an = 0;
        PeriodicSystem instance = PeriodicSystem.getInstance();
        Element expResult = new Element(
                33,
                "Arsen",
                new Temperature(613d),
                new Temperature("sublimiert")
        );
        Element result = instance.getElement(33);
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of getRandomElement method, of class PeriodicSystem.
     */
    @Test
    public void testGetRandomElement() {
        System.out.println("getRandomElement");
        PeriodicSystem instance = PeriodicSystem.getInstance();
        Element result = instance.getRandomElement();
        assertTrue(result instanceof Element);
    }
    
}
