/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.chemicals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stefan
 */
public class TemperatureTest {
    
    public TemperatureTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getType method, of class Temperature.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Temperature instance = new Temperature("normal");
        String expResult = "normal";
        String result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setType method, of class Temperature.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        String type = "normal";
        Temperature instance = new Temperature();
        instance.setType(type);
    }

    /**
     * Test of value method, of class Temperature.
     */
    @Test
    public void testValue_0args() {
        System.out.println("value");
        Temperature instance = new Temperature();
        Double expResult = null;
        Double result = instance.value();
        assertEquals(expResult, result);
    }

    /**
     * Test of value method, of class Temperature.
     */
    @Test
    public void testValue_Double() {
        System.out.println("value");
        Double value = 200d;
        Temperature instance = new Temperature();
        instance.value(value);
    }

    /**
     * Test of toString method, of class Temperature.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Temperature instance = new Temperature(200d);
        String expResult = "Temperature{type=normal, value=200.0}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
